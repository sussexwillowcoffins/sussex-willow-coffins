We hand craft British made natural wicker willow coffins, from our workshop in Sussex. Bespoke wicker coffin designs undertaken besides our green coffin and basket casket designs. We take utmost care to ensure all materials are as natural and locally sourced as possible.

Website : https://sussexwillowcoffins.co.uk/
